package com.debug.java.technology.one.server.thread;/**
 * Created by Administrator on 2020/11/26.
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/11/26 15:58
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyConnection implements Serializable{

    private String name;

}