package com.debug.java.technology.one.server.thread;/**
 * Created by Administrator on 2020/11/26.
 */

import com.beust.jcommander.internal.Maps;

import java.util.Map;
import java.util.concurrent.Callable;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/11/26 9:57
 **/
public class ProductThread implements Callable<Map<String,Object>>{

    private Map<String,Object> dataMap;

    @Override
    public Map<String, Object> call() throws Exception {
        System.out.println("---子线程在执行任务---");
        Thread.sleep(3000);

        dataMap=Maps.newHashMap();
        dataMap.put("id",10010);
        dataMap.put("name","steadyjack");
        dataMap.put("nickName","多隆");
        return dataMap;
    }
}