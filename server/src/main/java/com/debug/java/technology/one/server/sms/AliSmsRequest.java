package com.debug.java.technology.one.server.sms;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/6/4 9:50
 **/
@Data
public class AliSmsRequest implements Serializable{

    //要接受短信的手机号
    private String phoneNumbers;

    //申请的短信签名
    private String signName;

    //申请的模板编码
    private String templateCode;

    //短信发送参数-json格式的字符串，如{"code":123456}
    private String templateParam;

    //短信发送上行编码-按照官方建议的填就行
    private String smsUpExtendCode;

    //序列id-按照官方建议的填就行
    private String outId;

    public AliSmsRequest(String phoneNumbers, String signName, String templateCode, String templateParam) {
        this.phoneNumbers = phoneNumbers;
        this.signName = signName;
        this.templateCode = templateCode;
        this.templateParam = templateParam;
    }

    public AliSmsRequest() {
    }
}