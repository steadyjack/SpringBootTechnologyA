package com.debug.java.technology.one.server.service;/**
 * Created by Administrator on 2020/11/7.
 */

import com.debug.java.technology.one.server.dto.ArthasDto;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/11/7 21:13
 **/
@Service
public class SubmitService {

    public List<ArthasDto> getList(){
        List<ArthasDto> dtos= Lists.newLinkedList();
        dtos.add(new ArthasDto(1,"20201107001",15.5));
        dtos.add(new ArthasDto(2,"20201107002",25.5));
        dtos.add(new ArthasDto(3,"20201107003",16.5));
        dtos.add(new ArthasDto(4,"20201107004",18.5));
        return dtos;
    }

}