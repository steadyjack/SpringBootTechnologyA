package com.debug.java.technology.one.server.controller;/**
 * Created by Administrator on 2020/7/22.
 */

import com.debug.java.technology.one.api.response.BaseResponse;
import com.debug.java.technology.one.api.response.StatusCode;
import com.debug.java.technology.one.server.thread.MyConnectionPool;
import com.debug.java.technology.one.server.utils.CommonUtil;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/7/22 16:59
 **/
@RestController
    @RequestMapping("thread")
    public class ThreadController extends BaseController{


        @RequestMapping("num/random")
        public BaseResponse randomNum(){
            BaseResponse responses=new BaseResponse(StatusCode.Success);
            Map<String,Object> resMap= Maps.newHashMap();

            for (int i=0;i<100;i++){
                System.out.println(CommonUtil.getCurrDate());
                //resMap.put(getCurrDate(), RandomStringUtils.randomNumeric(3));
            }
            return responses;
        }



        @Autowired
        private MyConnectionPool myConnPool;

        @RequestMapping("pool")
        public BaseResponse getPool() {
            BaseResponse responses=new BaseResponse(StatusCode.Success);

            try {
                //responses.setData(myConnPool.getConn("datasource_mysql"));

                //responses.setData(myConnPool.getConnA("datasource_mysql"));

                //responses.setData(myConnPool.getConnB("datasource_mysql"));

                responses.setData(myConnPool.getConnC("datasource_mysql"));
            }catch (Exception e){
            }

            return responses;
        }


}






















