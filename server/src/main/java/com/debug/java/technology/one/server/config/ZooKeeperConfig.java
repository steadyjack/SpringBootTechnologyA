package com.debug.java.technology.one.server.config;/**
 * Created by Administrator on 2020/11/22.
 */

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/11/22 20:44
 **/
@Configuration
public class ZooKeeperConfig {

    private static final String ZK_ADDRESS = "127.0.0.1:2181";

    @Bean
    public CuratorFramework curatorFramework(){
        //如果获取链接失败，则重试3次，每次间隔2s
        RetryPolicy policy=new RetryNTimes(3,2000);
        //获取链接到zk服务的客户端实例
        CuratorFramework curatorFramework= CuratorFrameworkFactory.builder()
                .connectString(ZK_ADDRESS).sessionTimeoutMs(5000).connectionTimeoutMs(10000)
                .retryPolicy(policy).build();
        //启动客户端
        curatorFramework.start();
        return curatorFramework;
    }

}


