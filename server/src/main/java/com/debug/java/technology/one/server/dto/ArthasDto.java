package com.debug.java.technology.one.server.dto;/**
 * Created by Administrator on 2020/11/7.
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/11/7 20:44
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArthasDto implements Serializable{

    private Integer id;

    private String orderNo;

    private Double orderPrice;

}






























