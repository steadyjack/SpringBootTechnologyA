package com.debug.java.technology.one.server.dto;/**
 * Created by Administrator on 2020/7/22.
 */

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/7/22 18:01
 **/
@Data
public class MailDto implements Serializable{

    //主题
    @NotBlank(message = "邮件主题不能为空！")
    private String subject;

    //内容
    @NotBlank(message = "邮件内容不能为空！")
    private String content;

    //接受者
    @NotBlank(message = "邮件接受者不能为空！")
    private String tos;


    private String msgId;
}