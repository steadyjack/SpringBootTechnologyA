package com.debug.java.technology.one.server.excel;/**
 * Created by Administrator on 2020/4/9.
 */

import cn.hutool.core.lang.Snowflake;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.debug.java.technology.one.model.entity.WorkerInfo;
import com.debug.java.technology.one.server.dto.ExcelDemoData;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,
 * 然后里面用到spring可以构造方法传进去
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/4/9 9:30
 **/
public class ExcelDemoListener extends AnalysisEventListener<ExcelDemoData>{

    private static final Logger log= LoggerFactory.getLogger(ExcelDemoListener.class);

    private static final Snowflake SNOW_FLAKE=new Snowflake(2,3);

    //每65535条存储一次DB
    private static final int Batch_Count=50000;

    private List<WorkerInfo> list= Lists.newLinkedList();


    private ExcelService excelService;

    public ExcelDemoListener(ExcelService excelService) {
        this.excelService = excelService;
    }

    //每一条数据的解析都会调用invoke
    @Override
    public void invoke(ExcelDemoData data, AnalysisContext analysisContext) {
        //log.info("--解析到数据：{}",data);

        WorkerInfo info=new WorkerInfo(randomCode(),data.getName(),data.getBirthDay(),data.getMoney());
        list.add(info);

        //达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (list.size() >= Batch_Count){
            saveData();

            //存储完成清理 list
            list.clear();
        }
    }

    //所有数据解析完成了 都会来调用
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        //这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        log.info("所有数据解析完成！");
    }

    //存储数据到DB
    private void saveData() {
        log.info("现在有 {} 条数据 -> 开始存储数据库！", list.size());
        excelService.saveBatchData(list);

        log.info("---存储数据库成功---");
    }

    //获取编码
    private String randomCode(){
        return SNOW_FLAKE.nextIdStr();
    }
}
























