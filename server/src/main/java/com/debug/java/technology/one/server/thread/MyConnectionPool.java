package com.debug.java.technology.one.server.thread;/**
 * Created by Administrator on 2020/11/26.
 */

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 模拟 xx链接池 - 池化设计思想
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/11/26 16:01
 **/
@Component
@Slf4j
public class MyConnectionPool {

    private ConcurrentHashMap<String,MyConnection> connMap=new ConcurrentHashMap<>();

    //获取链接
    public MyConnection getConn(final String key){
        MyConnection conn=connMap.get(key);
        if (conn!=null){
            return conn;
        }
        conn=createConn(key);
        connMap.putIfAbsent(key,conn);
        return conn;
    }

    //获取链接-synchronized同步的方式-虽牺牲性能，但没有浪费由于大量创建的链接所占用的空间资源 - 独占/悲观的方式
    public synchronized MyConnection getConnA(final String key){
        MyConnection conn=connMap.get(key);
        if (conn!=null){
            return conn;
        }
        conn=createConn(key);
        connMap.putIfAbsent(key,conn);
        return conn;
    }

    @Autowired
    private CuratorFramework client;

    //基于zk的分布式锁 ~ 这种就集成、依赖了第三方的中间件(不具备独立对外提供服务的特性)
    public MyConnection getConnB(final String key) throws Exception{
        MyConnection conn=null;
        conn=connMap.get(key);
        if (conn!=null){
            return conn;
        }

        InterProcessMutex mutex=new InterProcessMutex(client,"/pool/V4");
        try {
            if (mutex.acquire(6,TimeUnit.SECONDS)){
                conn=connMap.get(key);
                if (conn!=null){
                    return conn;
                }

                conn=createConn(key);
                connMap.putIfAbsent(key,conn);
            }
        }catch (Exception e){
        }finally {
            mutex.release();
        }
        return conn;
    }

    private ConcurrentHashMap<String,FutureTask<MyConnection>> connHashMap=new ConcurrentHashMap<>();

    //基于futureTask
    public MyConnection getConnC(final String key) throws Exception{
        FutureTask<MyConnection> futureTask=connHashMap.get(key);
        if (futureTask!=null){
            return futureTask.get();
        }

        //多线程访问这段代码都可以执行，即都创建了 “获取链接” 的任务，但是注意：此时还没真正地执行获取链接对象实例
        Callable<MyConnection> callable= () -> createConn(key);
        FutureTask<MyConnection> newTask= new FutureTask<>(callable);

        //同一时刻，对于同个key并发的多个线程只有一个可以成功此行代码，即  putIfAbsent ,当返回null时表示现在本地映射map还没有该key对应的task，否则调取
        //get堵塞式等待获取结果即可
        futureTask=connHashMap.putIfAbsent(key,newTask);
        if (futureTask==null){
            futureTask=newTask;
            futureTask.run();
        }
        return futureTask.get();
    }

    //创建链接
    private MyConnection createConn(final String key){
        log.info("---{} 创建链接对象---",Thread.currentThread().getName());
        return new MyConnection(key);
    }

}





















