package com.debug.java.technology.one.server.mq;/**
 * Created by Administrator on 2020/7/22.
 */

import com.debug.java.technology.one.model.entity.MsgLog;
import com.debug.java.technology.one.model.mapper.MsgLogMapper;
import com.debug.java.technology.one.server.dto.MailDto;
import com.debug.java.technology.one.server.enums.Constant;
import com.debug.java.technology.one.server.service.MailService;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/7/22 18:10
 **/
@Component
public class MqListener {

    private static final Logger log= LoggerFactory.getLogger(MqListener.class);

    private static final ArrayBlockingQueue BLOCKING_QUEUE=new ArrayBlockingQueue(6,true);

    private static final ThreadPoolExecutor.CallerRunsPolicy POLICY=new ThreadPoolExecutor.CallerRunsPolicy();

    private static final ThreadPoolExecutor EXECUTOR=new ThreadPoolExecutor(2,4,10, TimeUnit.SECONDS,BLOCKING_QUEUE,POLICY);


    @Autowired
    private MailService mailService;

    @Autowired
    private MsgLogMapper msgLogMapper;



    //监听消费发送邮件消息
    @RabbitListener(queues = {"${mq.email.queue}"},containerFactory = "multiListenerContainer")
    public void consumeMail(Message message, Channel channel) throws Exception{
        log.info("---监听消费发送邮件消息---开始处理中...");
        Long deliverTag=message.getMessageProperties().getDeliveryTag();

        try {
            MailDto dto=new Gson().fromJson(new String(message.getBody()), MailDto.class);
            log.info("---监听到消息：{}",dto);

            //消息确认消费：第一个参数=消息的唯一标识；第二个参数=是否允许批量接收并处理消息
            channel.basicAck(deliverTag,false);
        }catch (Exception e){
            e.printStackTrace();

            //消息拒绝消费：第一、二个参数同上；第三个参数：表示是否重入队列
            channel.basicNack(deliverTag,false,true);
        }
    }


    //监听消费发送邮件消息-v2
    @RabbitListener(queues = {"${mq.email.v2.queue}"},containerFactory = "multiListenerContainer")
    public void consumeMailV2(Message message, Channel channel) throws Exception{
        log.info("---监听消费发送邮件消息-v2---开始处理中...");
        Long deliverTag=message.getMessageProperties().getDeliveryTag();

        Integer tryCount=0;
        String msgId="";
        try {
            MailDto dto=new Gson().fromJson(new String(message.getBody()), MailDto.class);
            msgId=dto.getMsgId();
            log.info("---监听到消息-v2：{}",dto);

            //maxTry=msgLogMapper.selectMaxTry(dto.getMsgId());

            /*此处可以加上一层分布式锁，保证超高并发时处理消息的原理操作 ~ 读者自行实现，有问题可以随时交流*/

            //防止重复投递 - 保证幂等性
            MsgLog msgLog=msgLogMapper.selectByPrimaryKey(dto.getMsgId());
            if (msgLog!=null && !Constant.MsgStatus.ConsumeSuccess.getStatus().equals(msgLog.getStatus()) && msgLog.getTryCount()<Constant.MsgTryCount){
                tryCount=msgLog.getTryCount();

                //核心业务逻辑【消息处理~发送邮件(同步；异步)】
                boolean res=mailService.sendSimpleEmail(msgId,dto.getSubject(),dto.getContent(),dto.getTos());
                if (res){
                    //确认消费
                    channel.basicAck(deliverTag,false);
                    return;
                }
            }

        }catch (Exception e){
            e.printStackTrace();

            //出现异常时可以走重试机制
            if (tryCount<Constant.MsgTryCount){
                Thread.sleep(8000);

                channel.basicNack(deliverTag,false,true);
                msgLogMapper.updateMaxTry(msgId);
                return;
            }
        }

        //当走到这一步时代表消息已被消费（status=3）、重试次数达到上限 等情况-但不管是哪种情况，都需要将消息从队列中移除，防止下次项目重启时重新监听消费
        channel.basicAck(deliverTag,false);
    }

//    public Boolean sendMail(MailDto dto) throws Exception{
//        Callable<Boolean> callable= () -> mailService.sendSimpleEmail(dto.getMsgId(),dto.getSubject(),dto.getContent(),dto.getTos());
//        FutureTask<Boolean> task=new FutureTask<Boolean>(callable);
//        EXECUTOR.submit(task);
//        return task.get();
//    }
}


































