package com.debug.java.technology.one.server.controller;/**
 * Created by Administrator on 2020/7/22.
 */

import com.debug.java.technology.one.api.response.BaseResponse;
import com.debug.java.technology.one.api.response.StatusCode;
import com.debug.java.technology.one.model.mapper.InterviewMapper;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/7/22 17:00
 **/
@RestController
@RequestMapping("one")
public class OneController extends BaseController{

    @Autowired
    private InterviewMapper interviewMapper;


    //Demo案例
    @RequestMapping("hello/world")
    public BaseResponse helloWorld(){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            Map<String,Object> dataMap= Maps.newHashMap();
            dataMap.put("msg","Spring Boot核心技术实战系列一");
            dataMap.put("info",interviewMapper.selectByPrimaryKey(1));

            response.setData(dataMap);
        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }


}



















