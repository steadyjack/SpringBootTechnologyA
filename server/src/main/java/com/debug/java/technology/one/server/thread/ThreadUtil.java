package com.debug.java.technology.one.server.thread;/**
 * Created by Administrator on 2020/11/26.
 */

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/11/26 9:55
 **/
public class ThreadUtil {

    private static final Map<String,Object> dataMap=Maps.newHashMap();

    public static void main(String[] args) throws Exception{
//        ArrayBlockingQueue queue=new ArrayBlockingQueue(2);
//        ExecutorService executorService=new ThreadPoolExecutor(2,4,1, TimeUnit.MINUTES,queue);
//
//        //ExecutorService executorService=Executors.newCachedThreadPool();
//        Future<Map<String,Object>> future=executorService.submit(new ProductThread());
//        executorService.shutdown();
//
//        //Thread.sleep(1000);
//        System.out.println("---主线程在执行任务---");
//
//        Map<String,Object> map=future.get();
//        System.out.println("子线程执行结果："+map);
//
//        //System.out.println("---所有任务执行完毕---");


//        Thread thread=new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("---子线程正在执行---"+Thread.currentThread().getName());
//
//                //Map<String,Object> dataMap=Maps.newHashMap();
//                dataMap.put("id",10010);
//                dataMap.put("name","steadyjack");
//                dataMap.put("nickName","多隆");
//                System.out.println("---子线程执行后得到的结果："+dataMap);
//            }
//        });
//        try {
//            thread.start();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//        System.out.println("---主线程正在执行---"+Thread.currentThread().getName());


//
//        dataMap.put("id",10010);
//        for (int i = 1; i <= 10; i++) {
//            Thread thread = new Thread(() -> {
//                Integer tmp= (Integer) dataMap.get("id");
//                dataMap.put("id",tmp+1);
//            });
//            try {
//                thread.start();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        System.out.println(dataMap);

        /*ArrayBlockingQueue queue=new ArrayBlockingQueue(2);
        ExecutorService executorService=new ThreadPoolExecutor(2,4,1, TimeUnit.MINUTES,queue);
        FutureTask<Map<String,Object>> futureTask=new FutureTask<Map<String, Object>>(new ProductThread());
        executorService.execute(futureTask);
        Map<String,Object> resMap=futureTask.get();
        executorService.shutdown();
        System.out.println("---主线程正在执行任务---"+Thread.currentThread().getName());

        System.out.println("--子线程执行任务后得到的结果："+resMap);*/


        String str="203.208.60.0\n" +
                "203.208.60.1\n" +
                "203.208.60.10\n" +
                "203.208.60.100\n" +
                "203.208.60.101\n" +
                "203.208.60.102\n" +
                "203.208.60.103\n" +
                "203.208.60.104\n" +
                "203.208.60.105\n" +
                "203.208.60.106\n" +
                "203.208.60.107\n" +
                "203.208.60.108\n" +
                "203.208.60.109\n" +
                "203.208.60.11\n" +
                "203.208.60.110\n" +
                "203.208.60.111\n" +
                "203.208.60.112\n" +
                "203.208.60.113\n" +
                "203.208.60.114\n" +
                "203.208.60.115\n" +
                "203.208.60.116\n" +
                "203.208.60.117\n" +
                "203.208.60.118\n" +
                "203.208.60.119\n" +
                "203.208.60.12\n" +
                "203.208.60.120\n" +
                "203.208.60.121\n" +
                "203.208.60.122\n" +
                "203.208.60.123\n" +
                "203.208.60.124\n" +
                "203.208.60.125\n" +
                "203.208.60.126\n" +
                "203.208.60.127\n" +
                "203.208.60.13\n" +
                "203.208.60.14\n" +
                "203.208.60.15\n" +
                "203.208.60.16\n" +
                "203.208.60.17\n" +
                "203.208.60.18\n" +
                "203.208.60.19\n" +
                "203.208.60.2\n" +
                "203.208.60.20\n" +
                "203.208.60.21\n" +
                "203.208.60.23\n" +
                "203.208.60.24\n" +
                "203.208.60.25\n" +
                "203.208.60.26\n" +
                "203.208.60.27\n" +
                "203.208.60.28\n" +
                "203.208.60.29\n" +
                "203.208.60.3\n" +
                "203.208.60.30\n" +
                "203.208.60.31\n" +
                "203.208.60.32\n" +
                "203.208.60.33\n" +
                "203.208.60.34\n" +
                "203.208.60.35\n" +
                "203.208.60.36\n" +
                "203.208.60.37\n" +
                "203.208.60.38\n" +
                "203.208.60.39\n" +
                "203.208.60.40\n" +
                "203.208.60.41\n" +
                "203.208.60.42\n" +
                "203.208.60.43\n" +
                "203.208.60.44\n" +
                "203.208.60.45\n" +
                "203.208.60.46\n" +
                "203.208.60.47\n" +
                "203.208.60.48\n" +
                "203.208.60.49\n" +
                "203.208.60.5\n" +
                "203.208.60.50\n" +
                "203.208.60.51\n" +
                "203.208.60.52\n" +
                "203.208.60.53\n" +
                "203.208.60.54\n" +
                "203.208.60.55\n" +
                "203.208.60.56\n" +
                "203.208.60.57\n" +
                "203.208.60.58\n" +
                "203.208.60.59\n" +
                "203.208.60.6\n" +
                "203.208.60.60\n" +
                "203.208.60.61\n" +
                "203.208.60.62\n" +
                "203.208.60.63\n" +
                "203.208.60.64\n" +
                "203.208.60.65\n" +
                "203.208.60.66\n" +
                "203.208.60.67\n" +
                "203.208.60.68\n" +
                "203.208.60.69\n" +
                "203.208.60.7\n" +
                "203.208.60.70\n" +
                "203.208.60.71\n" +
                "203.208.60.72\n" +
                "203.208.60.73\n" +
                "203.208.60.74\n" +
                "203.208.60.75\n" +
                "203.208.60.76\n" +
                "203.208.60.77\n" +
                "203.208.60.78\n" +
                "203.208.60.79\n" +
                "203.208.60.8\n" +
                "203.208.60.80\n" +
                "203.208.60.81\n" +
                "203.208.60.82\n" +
                "203.208.60.83\n" +
                "203.208.60.84\n" +
                "203.208.60.85\n" +
                "203.208.60.86\n" +
                "203.208.60.87\n" +
                "203.208.60.88\n" +
                "203.208.60.89\n" +
                "203.208.60.9\n" +
                "203.208.60.90\n" +
                "203.208.60.91\n" +
                "203.208.60.92\n" +
                "203.208.60.93\n" +
                "203.208.60.94\n" +
                "203.208.60.95\n" +
                "203.208.60.96\n" +
                "203.208.60.97\n" +
                "203.208.60.98\n" +
                "203.208.60.99";
        str=str.replaceAll("\n",",");
        System.out.println(str);

    }
}































