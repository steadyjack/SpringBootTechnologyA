package com.debug.java.technology.one.server.annotation;

import java.lang.annotation.*;

/**
 * @author 修罗debug
 * @date 2020/10/20 22:52
 * @link 微信：debug0868  QQ：1948831260
 * @blog fightjava.com
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {

}
