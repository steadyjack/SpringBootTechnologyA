package com.debug.java.technology.one.server.annotation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 修罗debug
 * @date 2020/10/21 23:23
 * @link 微信：debug0868  QQ：1948831260
 * @blog fightjava.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RepeatSubmitCacheDto implements Serializable{

    //请求体的数据
    private String requestData;

    //当前时间-用于比较有效期
    private Long currTime;

    private String requestUrl;
}
