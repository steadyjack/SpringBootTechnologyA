package com.debug.java.technology.one.server.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 修罗debug
 * @date 2020/10/21 22:08
 * @link 微信：debug0868  QQ：1948831260
 * @blog fightjava.com
 */
@Data
public class RegisterDto implements Serializable{

    private String userName;

    private String nickName;

    private Integer age;
}
