package com.debug.java.technology.one.server.controller;

import com.debug.java.technology.one.api.response.BaseResponse;
import com.debug.java.technology.one.api.response.StatusCode;
import com.debug.java.technology.one.server.annotation.RepeatSubmit;
import com.debug.java.technology.one.server.dto.ArthasDto;
import com.debug.java.technology.one.server.dto.RegisterDto;
import com.debug.java.technology.one.server.service.SubmitService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author 修罗debug
 * @date 2020/10/21 22:06
 * @link 微信：debug0868  QQ：1948831260
 * @blog fightjava.com
 */
@RestController
@RequestMapping("submit")
public class SubmitController extends BaseController{

    //用户注册
    @RepeatSubmit
    @PostMapping("register")
    public BaseResponse register(@RequestBody RegisterDto dto) throws Exception{
        BaseResponse response=new BaseResponse(StatusCode.Success);
        //log.info("用户注册，提交上来的请求信息为：{}",dto);

        //将用户信息插入到db

        response.setData(dto);
        return response;
    }




    @Autowired
    private SubmitService submitService;

    //arthas诊断
    @PostMapping("register/v2")
    public BaseResponse registerV2(@RequestBody RegisterDto dto) throws Exception{
        BaseResponse response=new BaseResponse(StatusCode.Success);
        //log.info("用户注册，提交上来的请求信息为：{}",dto);

        //将用户信息插入到db

        Map<String,Object> resMap= Maps.newHashMap();

        resMap.put("userInfo",dto);
        resMap.put("dtos",submitService.getList());

        response.setData(resMap);
        return response;
    }
}



























