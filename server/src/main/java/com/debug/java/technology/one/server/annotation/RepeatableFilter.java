package com.debug.java.technology.one.server.annotation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 重复提交过滤器-在拦截器执行之前先执行此过滤器，用于将request/response等对象封装到自定义的请求包装器中
 * 
 */
public class RepeatableFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException{
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException{
        ServletRequest requestWrapper = null;
        if (request instanceof HttpServletRequest && StringUtils.equalsAnyIgnoreCase(request.getContentType(),MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE)){
            requestWrapper = new RepeatedlyRequestWrapper((HttpServletRequest) request, response);
        }if (null == requestWrapper){
            chain.doFilter(request, response);
        }else{
            chain.doFilter(requestWrapper, response);
        }
    }

    @Override
    public void destroy(){

    }
}
