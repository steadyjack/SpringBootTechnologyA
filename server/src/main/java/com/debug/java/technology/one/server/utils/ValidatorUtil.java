package com.debug.java.technology.one.server.utils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.validation.BindingResult;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

/**
 * 统一校验前端参数的工具
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/3/12 10:46
 **/
public class ValidatorUtil {

    //TODO:统一校验前端传递过来的参数
    public static String checkResult(BindingResult result){
        StringBuilder sb=new StringBuilder("");
        if (result.hasErrors()){
            result.getAllErrors().forEach(error -> sb.append(error.getDefaultMessage()).append("\n"));
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception{
        /*final String url="/hello/world";
        Cache<String,String> cache=CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();
        cache.put(url,"hello world!");
        System.out.print("第1次："+cache.getIfPresent(url)+"\n");
        Thread.sleep(3000);
        System.out.print("第2次："+cache.getIfPresent(url)+"\n");
        Thread.sleep(3000);
        System.out.print("第3次："+cache.getIfPresent(url)+"\n");*/


        /*HashMap<String,Object> resMap=new HashMap<>(32);
        resMap.put("k1","v1");
        resMap.put("k2","v2");
        resMap.put("k3","v3");
        System.out.println(resMap.size());

        Object v=resMap.get("k3");
        System.out.println(v);*/

        /*List<Map<String,Object>> list= Lists.newLinkedList();

        Map<String,Object> dataMap=Maps.newHashMap();
        dataMap.put("id",10010);
        dataMap.put("name","小姐姐");
        dataMap.put("age",21);
        list.add(dataMap);

        dataMap=Maps.newHashMap();
        dataMap.put("id",10011);
        dataMap.put("name","小姐姐2");
        dataMap.put("age",22);
        list.add(dataMap);

        System.out.println("list<Map>的取值："+list);

        List<Man> manList=Lists.newLinkedList();
        list.forEach(map -> {
            Man man=new Man();
            try {
                BeanUtils.populate(man,map);
            } catch (Exception e) {
                e.printStackTrace();
            }
            manList.add(man);
        });
        System.out.println("list<Object>的取值："+manList);*/


        //使用线程池并发处理逻辑:5个线程随机生成N个随机数
        ForkJoinPool joinPool=new ForkJoinPool(5);
        joinPool.execute(() -> IntStream.rangeClosed(1,10).parallel().forEach(value -> {
            ThreadLocalRandom random=ThreadLocalRandom.current();
            System.out.println(Thread.currentThread().getName()+" -- "+random.nextInt(10));

        }));
        joinPool.shutdown();
        joinPool.awaitTermination(1,TimeUnit.HOURS);
    }
}

































