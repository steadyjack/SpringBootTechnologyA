package com.debug.java.technology.one.server.config;

import com.debug.java.technology.one.server.annotation.RepeatSubmitInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 修罗debug
 * @date 2020/10/21 22:18
 * @link 微信：debug0868  QQ：1948831260
 * @blog fightjava.com
 */
@Component
public class CustomWebConfig implements WebMvcConfigurer{

    @Autowired
    private RepeatSubmitInterceptor submitInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(submitInterceptor);
    }
}
