package com.debug.java.technology.one.server.sms;/**
 * Created by Administrator on 2020/6/4.
 */

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/6/4 9:02
 **/
@Configuration
@ConfigurationProperties(prefix = "ali.sms")
@Data
public class AliSmsProperty {

    private String accessKeyId;

    private String accessSecret;

    private String regionId;

    private String sysDomain;

    private String sysVersion;

    private String sysAction;

    private Boolean enabled;
}