package com.debug.java.technology.one.server.sms;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 阿里云短信服务
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/6/4 9:43
 **/
@Service
public class AliSmsService {

    private static final Logger log= LoggerFactory.getLogger(AliSmsService.class);

    private static final String Channel="aliyun";

    @Autowired
    private AliSmsProperty aliSmsProperty;

    @Autowired
    private ObjectMapper objectMapper;

    //发送通用消息
    public void sendMsg(AliSmsRequest smsRequest,final Long sendId) throws Exception{
        if (!aliSmsProperty.getEnabled()){
            return;
        }

        //参数校验

        //短信配置初始化
        DefaultProfile profile=DefaultProfile.getProfile(aliSmsProperty.getRegionId(),aliSmsProperty.getAccessKeyId(),aliSmsProperty.getAccessSecret());
        IAcsClient client=new DefaultAcsClient(profile);

        //构造短信发送参数
        CommonRequest request=new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(aliSmsProperty.getSysDomain());
        request.setSysVersion(aliSmsProperty.getSysVersion());
        request.setSysAction(aliSmsProperty.getSysAction());

        request.putQueryParameter("RegionId", aliSmsProperty.getRegionId());
        request.putQueryParameter("PhoneNumbers", smsRequest.getPhoneNumbers());
        request.putQueryParameter("SignName", smsRequest.getSignName());
        request.putQueryParameter("TemplateCode", smsRequest.getTemplateCode());
        request.putQueryParameter("TemplateParam", smsRequest.getTemplateParam());

        request.putQueryParameter("SmsUpExtendCode", RandomStringUtils.randomNumeric(4));
        request.putQueryParameter("OutId", smsRequest.getTemplateCode()+"_"+RandomStringUtils.randomAlphanumeric(8));

        //发送短信
        CommonResponse response = client.getCommonResponse(request);
        log.info("----阿里云短信发送结果：{},{},{}----",response.getHttpStatus(),response.getHttpResponse(),response.getData());

        if (response!=null && StringUtils.isNotBlank(response.getData())){
            AliSmsResponse smsResponse=objectMapper.readValue(response.getData(),AliSmsResponse.class);
            if (smsResponse!=null && "OK".equals(smsResponse.getCode())){
                //创建一条短信发送成功的记录(记录到数据库DB，以用于后续的验证码验证)

            }else{
                //创建一条短信发送失败的记录

            }
        }
    }

}

























