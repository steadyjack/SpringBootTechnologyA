package com.debug.java.technology.one.server.controller;/**
 * Created by Administrator on 2020/4/8.
 */

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.debug.java.technology.one.api.response.BaseResponse;
import com.debug.java.technology.one.api.response.StatusCode;
import com.debug.java.technology.one.server.dto.ExcelDemoData;
import com.debug.java.technology.one.server.excel.ExcelDemoListener;
import com.debug.java.technology.one.server.excel.ExcelService;
import com.debug.java.technology.one.server.utils.WebUtil;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/4/8 21:31
 **/
@Controller
@RequestMapping("excel")
public class ExcelController{

    private static final String rootPath="E:\\shFiles\\excel\\";

    @Autowired
    private ExcelService excelService;


    //简单版本
    @RequestMapping("write/v1")
    @ResponseBody
    public BaseResponse writeExcel(@RequestParam Long total){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            String fileName="EasyExcel实战一~"+System.currentTimeMillis()+ExcelTypeEnum.XLS.getValue();
            List<ExcelDemoData> list=excelService.getDataV1(total);
            EasyExcel.write(rootPath+fileName,ExcelDemoData.class)
                    .excelType(ExcelTypeEnum.XLS)
                    .sheet(1,"第一个表")
                    .doWrite(list);

        }catch (Exception e){
            e.printStackTrace();
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //高级版本
    @RequestMapping("write/v2")
    @ResponseBody
    public BaseResponse writeExcelV2(@RequestParam Long total){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            //多个sheet
            String fileName="EasyExcel实战二~"+System.currentTimeMillis()+ ExcelTypeEnum.XLS.getValue();
            ExcelWriter excelWriter = EasyExcel.write(rootPath+fileName).build();
            WriteSheet writeSheet;

            //去调用写入,这里我调用了N次，实际使用时根据数据库分页的总的页数来。这里最终会写到5个sheet里面
            List<List<ExcelDemoData>> resList=excelService.getDataV2(total);
            for (int i = 0; i < resList.size(); i++) {
                writeSheet = EasyExcel.writerSheet(i, "表" + i).head(ExcelDemoData.class).build();
                excelWriter.write(resList.get(i), writeSheet);
            }
            //关闭流
            excelWriter.finish();

        }catch (Exception e){
            e.printStackTrace();
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

    //高级版本-web下载
    @GetMapping("write/v3")
    public @ResponseBody void writeExcelV3(@RequestParam Long total,HttpServletResponse webRes) throws Exception{
        String fileName="EasyExcel实战三~"+System.currentTimeMillis()+ExcelTypeEnum.XLSX.getValue();
        List<ExcelDemoData> list=excelService.getDataV1(total);

        webRes.setContentType("application/vnd.ms-excel");
        webRes.setCharacterEncoding("utf-8");
        fileName=URLEncoder.encode(fileName,"UTF-8");
        webRes.setHeader("Content-disposition", "attachment;filename=" + fileName);
        EasyExcel.write(webRes.getOutputStream(),ExcelDemoData.class).sheet("第一个表").doWrite(list);
    }


    //高级版本-web下载-2
    @GetMapping("write/v4")
    public void writeExcelV4(@RequestParam Long total,HttpServletResponse webRes) throws Exception{
        try {
            String fileName="EasyExcel实战三~"+System.currentTimeMillis()+ExcelTypeEnum.XLSX.getValue();
            List<ExcelDemoData> list=excelService.getDataV1(total);

            webRes.setContentType("application/vnd.ms-excel");
            webRes.setCharacterEncoding("utf-8");
            fileName=URLEncoder.encode(fileName,"UTF-8");
            webRes.setHeader("Content-disposition", "attachment;filename=" + fileName);
            EasyExcel.write(webRes.getOutputStream(),ExcelDemoData.class).sheet("第一个表").doWrite(list);

            int i=1/0;
        }catch (Exception e) {
            //重置response
            webRes.reset();
            webRes.setContentType("application/json");
            webRes.setCharacterEncoding("utf-8");
            Map<String, String> map = new HashMap<String, String>();
            map.put("status", "failure");
            map.put("message", "下载文件失败" + e.getMessage());
            webRes.getWriter().println(new Gson().toJson(map));
        }
    }

    //高级版本-web下载-3
    @GetMapping("write/v5")
    public void writeExcelV5(@RequestParam Long total,HttpServletResponse webRes) throws Exception{
        String fileName="EasyExcel_Data_"+System.currentTimeMillis()+ ExcelTypeEnum.XLSX.getValue();

        ExcelWriter excelWriter = EasyExcel.write(rootPath+fileName).build();
        WriteSheet writeSheet;

        //去调用写入,这里我调用了N次，实际使用时根据数据库分页的总的页数来。这里最终会写到5个sheet里面
        List<List<ExcelDemoData>> resList=excelService.getDataV2(total);
        for (int i = 0; i < resList.size(); i++) {
            writeSheet = EasyExcel.writerSheet(i, "表" + i).head(ExcelDemoData.class).build();
            excelWriter.write(resList.get(i), writeSheet);
        }
        //关闭流
        excelWriter.finish();

        //从磁盘/服务器中读
        FileInputStream is=new FileInputStream(rootPath+fileName);
        WebUtil.downloadFileNIO(webRes,is,fileName);
    }










    /**导入Excel**/

    //初级版本
    @RequestMapping("read/v1")
    @ResponseBody
    public BaseResponse readV1(@RequestParam String fileName){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            //写法一
            EasyExcel.read(rootPath+fileName,ExcelDemoData.class,new ExcelDemoListener(excelService)).sheet().doRead();

            //写法二
            /*ExcelReader reader=EasyExcel.read(rootPath+fileName,ExcelDemoData.class,new ExcelDemoListener(excelService)).build();
            ReadSheet readSheet=EasyExcel.readSheet(0).build();
            reader.read(readSheet);
            reader.finish();*/
        }catch (Exception e){
            e.printStackTrace();
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }


    //高级版本
    @RequestMapping("read/v2")
    @ResponseBody
    public BaseResponse readV2(@RequestParam String fileName){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            EasyExcel.read(rootPath+fileName,ExcelDemoData.class,new ExcelDemoListener(excelService))
                    .doReadAll();

        }catch (Exception e){
            e.printStackTrace();
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());

        }
        return response;
    }

    //高级版本-web上传
    @PostMapping("read/v3")
    @ResponseBody
    public BaseResponse readV3(MultipartHttpServletRequest request){
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            MultipartFile file=request.getFile("appendix");
            EasyExcel.read(file.getInputStream(),ExcelDemoData.class,new ExcelDemoListener(excelService)).doReadAll();
        }catch (Exception e){
            e.printStackTrace();
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }

}

















