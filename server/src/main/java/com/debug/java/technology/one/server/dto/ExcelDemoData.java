package com.debug.java.technology.one.server.dto;/**
 * Created by Administrator on 2020/4/8.
 */

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/4/8 21:36
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor

@HeadRowHeight(20)
@ColumnWidth(25)
@ContentRowHeight(15)
public class ExcelDemoData implements Serializable{

    @ExcelProperty("姓名")
    private String name;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "生日")
    private Date birthDay;

    @NumberFormat("#.##")
    @ExcelProperty("个人资产")
    private Double money;

    //被忽略的字段
    @ExcelIgnore
    private Integer sex;
}

































