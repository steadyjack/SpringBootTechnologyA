package com.debug.java.technology.one.server.utils;

import com.google.common.base.Strings;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;


@Service
public class WebUtil {

    private static final Logger log= LoggerFactory.getLogger(WebUtil.class);


    //通用下载附件-io
    public static void downloadFile(HttpServletResponse response, InputStream is, String fileName) throws Exception{
        if(is == null || Strings.isNullOrEmpty(fileName)){
            return;
        }

        BufferedInputStream bis = null;
        OutputStream os = null;
        BufferedOutputStream bos = null;
        try{
            bis = new BufferedInputStream(is);
            os = response.getOutputStream();
            bos = new BufferedOutputStream(os);
            response.setContentType("application/octet-stream;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename="+new String(fileName.getBytes("utf-8"),"iso-8859-1"));
            byte[] buffer = new byte[10240];
            int len = bis.read(buffer);
            while(len != -1){
                bos.write(buffer, 0, len);
                len = bis.read(buffer);
            }

            bos.flush();
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            if(bis != null){
                try{
                    bis.close();
                }catch(IOException e){}
            }
            try{
                is.close();
            }catch(IOException e){}
        }
    }


    //通用的下载文件-nio
    public static void downloadFileNIO(HttpServletResponse response, FileInputStream is, final String fileName) throws Exception{
        //TODO:往响应流设置响应类型和响应头
        response.setContentType("application/octet-stream;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename="+new String(fileName.getBytes("utf-8"),"iso-8859-1"));

        //10KB
        int bufferSize=10240;
        byte[] byteArr=new byte[bufferSize];

        //6*10KB=‭61440‬
        FileChannel fileChannel=is.getChannel();
        ByteBuffer buffer=ByteBuffer.allocateDirect(61440);

        int nRead;
        int nGet;
        try {
            while ( (nRead=fileChannel.read(buffer)) !=-1){
                if (nRead==0){
                    continue;
                }

                buffer.position(0);
                buffer.limit(nRead);
                while (buffer.hasRemaining()){
                    nGet=Math.min(buffer.remaining(),bufferSize);

                    //TODO:从磁盘中读I - Input
                    buffer.get(byteArr,0,nGet);
                    //TODO:往浏览器响应流写O - Output
                    response.getOutputStream().write(byteArr);
                }

                buffer.clear();
            }
        }finally {
            buffer.clear();
            fileChannel.close();
            is.close();
        }
    }


    //下载Excel
    public static void downloadExcel(HttpServletResponse response, Workbook wb, String fileName) throws Exception{
        response.setHeader("Content-Disposition", "attachment;filename="+new String(fileName.getBytes("utf-8"),"iso-8859-1"));
        response.setContentType("application/ynd.ms-excel;charset=UTF-8");
        OutputStream out=response.getOutputStream();
        wb.write(out);
        out.flush();
        out.close();
    }
}

























