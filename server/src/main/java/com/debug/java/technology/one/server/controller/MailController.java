package com.debug.java.technology.one.server.controller;/**
 * Created by Administrator on 2020/7/22.
 */

import cn.hutool.core.lang.Snowflake;
import com.debug.java.technology.one.api.response.BaseResponse;
import com.debug.java.technology.one.api.response.StatusCode;
import com.debug.java.technology.one.model.entity.MsgLog;
import com.debug.java.technology.one.model.mapper.MsgLogMapper;
import com.debug.java.technology.one.server.dto.MailDto;
import com.debug.java.technology.one.server.enums.Constant;
import com.debug.java.technology.one.server.service.MailService;
import com.debug.java.technology.one.server.utils.ValidatorUtil;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.sun.tools.internal.jxc.ap.Const;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/7/22 17:59
 **/
@RestController
@RequestMapping("mail")
public class MailController extends BaseController{

    @Autowired
    private MailService mailService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Environment env;

    @Autowired
    private MsgLogMapper msgLogMapper;

    private static final Snowflake snowflake=new Snowflake(3,2);


    //发送邮件
    @RequestMapping(value = "send",method = RequestMethod.POST)
    public BaseResponse sendMail(@RequestBody @Validated MailDto dto, BindingResult result){
        String checkRes=ValidatorUtil.checkResult(result);
        if (StringUtils.isNotBlank(checkRes)){
            return new BaseResponse(StatusCode.InvalidParams.getCode(),checkRes);
        }
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            //mailService.sendSimpleEmail(dto.getSubject(),dto.getContent(),dto.getTos());

            rabbitTemplate.setExchange(env.getProperty("mq.email.exchange"));
            rabbitTemplate.setRoutingKey(env.getProperty("mq.email.routing.key"));
            Message msg=MessageBuilder.withBody(new Gson().toJson(dto).getBytes())
                    .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                    .build();
            rabbitTemplate.send(msg);

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }


    //发送邮件V2
    @RequestMapping(value = "send/v2",method = RequestMethod.POST)
    public BaseResponse sendMailV2(@RequestBody @Validated MailDto dto, BindingResult result){
        String checkRes=ValidatorUtil.checkResult(result);
        if (StringUtils.isNotBlank(checkRes)){
            return new BaseResponse(StatusCode.InvalidParams.getCode(),checkRes);
        }
        BaseResponse response=new BaseResponse(StatusCode.Success);
        try {
            MsgLog entity=new MsgLog();
            String msgId=snowflake.nextIdStr();

            entity.setMsgId(msgId);
            entity.setExchange(env.getProperty("mq.email.v2.exchange"));
            entity.setRoutingKey(env.getProperty("mq.email.v2.routing.key"));
            entity.setStatus(Constant.MsgStatus.Sending.getStatus());
            entity.setTryCount(0);
            entity.setCreateTime(DateTime.now().toDate());

            dto.setMsgId(msgId);
            final String msg=new Gson().toJson(dto);

            entity.setMsg(msg);
            //在发送消息之前先创建并入库
            msgLogMapper.insertSelective(entity);

            //发送消息
            rabbitTemplate.convertAndSend(entity.getExchange(), entity.getRoutingKey(),msg,new CorrelationData(entity.getMsgId()));

        }catch (Exception e){
            response=new BaseResponse(StatusCode.Fail.getCode(),e.getMessage());
        }
        return response;
    }
}





























