package com.debug.java.technology.one.server.mq;/**
 * Created by Administrator on 2020/7/29.
 */

import com.debug.java.technology.one.model.entity.MsgLog;
import com.debug.java.technology.one.model.mapper.MsgLogMapper;
import com.debug.java.technology.one.server.enums.Constant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Consumer;

/**
 * 定时任务重新拉取投递失败的消息~重新进行投递
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/7/29 17:26
 **/
@Component
public class MqScheduler {

    private static final Logger log= LoggerFactory.getLogger(MqScheduler.class);

    @Autowired
    private MsgLogMapper msgLogMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    //定时任务重新拉取并投递
    //@Scheduled(cron = "0/10 * * * * ?")
    public void reSendMsg(){
        try {
            String status= Constant.MsgStatus.Sending.getStatus()+","+Constant.MsgStatus.SendFail.getStatus();
            List<MsgLog> msgLogs=msgLogMapper.selectSendFailMsg(status);
            if (msgLogs!=null && !msgLogs.isEmpty()){
                msgLogs.forEach(msgLog -> {
                    if (StringUtils.isNotBlank(msgLog.getMsg()) && StringUtils.isNotBlank(msgLog.getExchange())
                            && StringUtils.isNotBlank(msgLog.getRoutingKey())){
                        //发送消息
                        rabbitTemplate.convertAndSend(msgLog.getExchange(), msgLog.getRoutingKey(),msgLog.getMsg(),
                                new CorrelationData(msgLog.getMsgId()));
                        log.info("----已成功重新投递消息，消息id={}",msgLog.getMsgId());
                    }
                });

            }

        }catch (Exception e){
            log.error("定时任务重新拉取投递失败的消息~重新进行投递~发生异常：",e.fillInStackTrace());
        }
    }

}
































