package com.debug.java.technology.one.server.enums;/**
 * Created by Administrator on 2020/7/28.
 */

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/7/28 16:44
 **/
public class Constant {

    public static final Integer MsgTryCount=3;

    //消息的状态：0投递中 1投递成功 2投递失败 3已消费
    public enum MsgStatus{

        Sending(0,"投递中"),
        SendSuccess(1,"投递成功"),
        SendFail(2,"投递失败"),
        ConsumeSuccess(3,"消费成功"),
        ConsumeFail(4,"消费失败"),

        ;

        private Integer status;
        private String msg;

        MsgStatus(Integer status, String msg) {
            this.status = status;
            this.msg = msg;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

}