package com.debug.java.technology.one.server.excel;/**
 * Created by Administrator on 2020/4/8.
 */

import com.debug.java.technology.one.model.entity.WorkerInfo;
import com.debug.java.technology.one.model.mapper.WorkerInfoMapper;
import com.debug.java.technology.one.server.dto.ExcelDemoData;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author:debug (SteadyJack)
 * @Link: weixin-> debug0868 qq-> 1948831260
 * @Date: 2020/4/8 21:32
 **/
@Service
public class ExcelService {

    private static final Logger log= LoggerFactory.getLogger(ExcelService.class);

    @Autowired
    private WorkerInfoMapper infoMapper;


    //获取数据
    public List<ExcelDemoData> getDataV1(Long total){
        List<ExcelDemoData> list=Lists.newLinkedList();
        /*list.add(new ExcelDemoData("修罗debug",new Date(),10.56,1));
        list.add(new ExcelDemoData("大圣",new Date(),20.56,0));
        list.add(new ExcelDemoData("毕玄",new Date(),30.56,1));*/

        ExcelDemoData data;
        for (Long i=1L;i<=total;i++){
            data=new ExcelDemoData(RandomStringUtils.randomAlphabetic(6),new Date(),Double.valueOf(String.valueOf(i)),1);
            list.add(data);
        }
        return list;
    }

    //获取数据-65535条数据为一个Sheet
    public List<List<ExcelDemoData>> getDataV2(Long total){
        List<List<ExcelDemoData>> resList=Lists.newLinkedList();

        List<ExcelDemoData> list=Lists.newLinkedList();
        ExcelDemoData data;

        for (Long i=1L;i<=total;i++){
            data=new ExcelDemoData(RandomStringUtils.randomAlphabetic(4),new Date(),Double.valueOf(String.valueOf(i))+0.25,1);
            list.add(data);
        }

        if (total>65535){
            Long count=(total % 65535 == 0) ? total/65535 : total/65535+1;
            for (Integer i=1;i<=count;i++){
                Integer start=(i-1)*65535;
                Long end=(total - start > 65535) ? (i*65535) : (total - 1);

                resList.add(list.subList(start,end.intValue()));
            }
        }else{
            resList.add(list);
        }
        return resList;
    }




    //批量存储信息
    public void saveBatchData(List<WorkerInfo> list){
        try {
            if (list!=null && list.size()>0){
                infoMapper.insertBatch(list);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}


























