# 程序员实战基地-SpringBoot与分布式核心技术栈-系列一

#### 介绍
做Java出身的小伙伴或许都知晓，如今IT Java领域已然进入微服务、分布式系统架构时代，作为一名Java攻城狮，最为欣喜、但也最为头疼的莫过于这一领域迭代更新、频现的技术是相当多且快速的，不时为发出“不要再更新啦，都快学不动了.....”

本仓库仍然是贯彻Debug一贯的撸码和学习风格、不限某一个领域、框架、业务或者技术，而是尽所能刨根问底，并以实际的代码加以实战实现，一起掌握所谓的高可用、高并发、系统架构设计云云之类的玩意。

当然，最能令自己感到欣喜的估计就是每次都有新的东西可学，新的东西可思考，不断鞭策自己前进！！！


#### 博文列表（按照文章发布时间先后顺序）
1、Java面试题~Java线程池实战总结一之百万数据的批量插入  
（1）链接：http://www.fightjava.com/web/index/blog/article/76    
（2）简介：对于从事Java开发的小伙伴而言，“线程池”一词应当不陌生，虽然在实际工作、项目实战中可能很少用过，但是在工作闲暇或吹水之余还是会听到他人在讨论，更有甚者，在跳槽面试等场合更是屡见不鲜，已然成为一道“必面题”。从本文开始我们将开启“Java线程池实战总结”系列文章的分享，帮助各位小伙伴认识、巩固并实战线程池的相关技术要点    
（3）经典截图：  
![](https://images.gitbook.cn/0859e120-15a4-11eb-87de-2b9d4b234df5 "15cm.jpg")    
![](https://images.gitbook.cn/16cdd4a0-15a4-11eb-8688-9938a68131a4 "15cm.jpg")   

2、Java面试题~从一个线程并发安全场景谈谈Java的锁、CAS算法以及ABA问题  
（1）链接：http://www.fightjava.com/web/index/blog/article/75  
（2）简介：对于“并发安全”，想必各位小伙伴都有所耳闻，它指的是“系统中某一时刻多个线程并发访问一段或者一个有线程安全性问题的代码、方法后，最终出现不正确的结果或者并非最初所预料的结果”。有问题并有对应的解决方案，而“并发安全性问题”对应的解决方案也无非是根据项目所处的环境而采取相应的措施，如单体环境下采用加synchronized同步锁、原子操作锁atomicXXX 以及 lock等方式；分布式环境下采取分布式锁（基于Redis、ZooKeeper、Redisson…）等方式加以解决。本文我们将以一个简单的场景：“模拟 网站访问的计数统计”为案例，介绍在多线程并发的场景下出现的结果及其相应的解决方案，包括synchronized、atomicXXX、lock以及介绍CAS算法和ABA相关的问题。  
（3）经典截图：  
![](https://images.gitbook.cn/b8ce22a0-cae5-11ea-ac55-4f1b62a062bc "15cm.jpg")    
  
3、Java面试题~消息中间件MQ如何保证消息可靠性投递与消息不丢失  
（1）链接：http://www.fightjava.com/web/index/blog/article/77       
（2）概要：本文我们将以实际项目中典型的业务场景：“发送邮件” 为案例，一起探讨RabbitMQ在发送消息时如何保证消息可靠性投递与消息不丢失   
（3）经典截图：    
![实现流程图](https://images.gitbook.cn/2bb20940-15a4-11eb-b719-5b5ee4c84eaa "15cm.jpg")  


4、Java技术干货实战~Java如何防止接口重复提交  
（1）链接：http://fightjava.com/web/index/blog/article/80       
（2）概要：正如本文标题所言，今天我们来聊一聊在Java应用系统中如何防止接口重复提交；简单地讲，这其实就是“重复提交”的话题，本文将从以下几个部分展开介绍： 1.“重复提交”简介与造成的后果;2.“防止接口重复提交”的实现思路;3.“防止接口重复提交”的代码实战    
（3）经典截图：    
![实现流程图](https://images.gitbook.cn/6d8a1d80-15a4-11eb-8688-9938a68131a4 "15cm.jpg")    

5、SpringBoot系列(22)：Java生成二维码的几种实现方式(基于Spring Boot)   
（1）链接：http://www.fightjava.com/web/index/blog/article/82          
（2）概要：在一些企业级应用系统中，有时候需要为产品或者商品生成特定的专属二维码，以供一些硬件设备或者用户在手机端扫码查看；其中，该二维码主要承载了该产品的相关核心信息，比如名称、简介、价格、单位、型号以及使用说明等等，本文将基于Spring Boot介绍两种生成二维码的实现方式，一种是基于Google开发工具包，另一种是基于Hutool来实现；      
（3）经典截图：      
![实现流程图](https://images.gitbook.cn/2af27050-3c94-11eb-bca2-45a203ae47ca "15cm.jpg")      


6、技术干货实战（2）- 聊一聊分布式系统全局唯一ID的几种实现方式      
（1）链接：http://www.fightjava.com/web/index/blog/article/83            
（2）概要：现如今可谓是微服务、分布式、IoT（物联网）横行的时代，作为一名开发者始终还是要保持一定的危机意识，特别是在日常的项目开发中，若是有机会接触到一些关于微服务、分布式下的应用场景，应当硬着头皮、排除万难，主动应承下来 上去大干一场；这期间不管结果如何，积累下来的经验将会让自己受益匪浅；而本文要介绍的“分布式全局唯一ID”便是一种典型的分布式应用场景！！！        
（3）经典截图：      
![实现流程图](https://images.gitbook.cn/44e448d0-3c94-11eb-865e-77f174dfc790 "15cm.jpg")        
![实现流程图](https://images.gitbook.cn/4d839ef0-3c94-11eb-97fb-ef8778312c25 "15cm.jpg")  


7、Java并发编程（1）- Callable、Future和FutureTask          
（1）链接：http://www.fightjava.com/web/index/blog/article/85              
（2）概要：撸过JavaSE（即Java基础技术栈）的小伙伴都知道，实现多线程有两种方式，一种是继承Thread,即extends Thread 然后实现其中的run()方法；另外一种是实现Runnable接口，即implements Runnable，然后实现其中的run()方法；仔细观察这两种方式，会发现这两者都不能返回线程异步执行完的结果，但在实际项目开发中却偶尔需要获取其中的返回结果，咋办嘞？于是乎Callable和Future就排上用场了，本文我们将对其做一番详尽的介绍！        
（3）经典截图：      


8、Java并发编程（2）- FutureTask详解与池化思想的设计和实战一          
（1）链接：http://www.fightjava.com/web/index/blog/article/87              
（2）概要：在Java并发编程领域，FutureTask可以说是一个非常强大的利器，它通过实现RunnableFuture接口间接拥有了Runnable和Future接口的相关特性，既可以用于充当线程执行的任务(Runnable)，也可以用于获取线程异步执行任务后返回的结果（Future）；本文将通过剖析解读FutureTask底层相关的核心源码，并基于FutureTask自设计并实战一款“池容器”，即池化思想的设计和实战！          
（3）经典截图：    
![实现流程图](https://images.gitbook.cn/5ed5fa40-3c94-11eb-9a6d-0397721adbe2 "15cm.jpg")  

9、技术干货实战（4）- 分布式集群部署模式下Nginx如何实现用户登录Session共享（含详细配置与代码实战）            
（1）链接：http://www.fightjava.com/web/index/blog/article/86                
（2）概要：最近有小伙伴催更，让debug多写点技术干货，以便多学习、巩固一些技能；没办法，debug也只好应承下来，再忙也要挤出时间撸一撸，以对得起时常关注debug的那些看官老爷们！ 本文将重点介绍：Nginx如何进行配置从而实现用户登录成功后Session共享的功能，其中我们将以“企业权限管理平台”为例，加入Redis最终真正实现Session共享的效果（真正的代码落地哈！）！        
（3）经典截图：    
![实现流程图](https://images.gitbook.cn/9df33440-3c94-11eb-bdef-0fdc4009ed3d "15cm.jpg")  
![实现流程图](https://images.gitbook.cn/afc6f490-3c94-11eb-bc92-7dc6e658314b "15cm.jpg")  
![实现流程图](https://images.gitbook.cn/b8c64500-3c94-11eb-a525-016c954e9e10 "15cm.jpg")  
![实现流程图](https://images.gitbook.cn/c4a07ad0-3c94-11eb-a525-016c954e9e10 "15cm.jpg")  
![实现流程图](https://images.gitbook.cn/cae95dd0-3c94-11eb-865e-77f174dfc790 "15cm.jpg")  
![实现流程图](https://images.gitbook.cn/d49bf8b0-3c94-11eb-97fb-ef8778312c25 "15cm.jpg")  





#### 课程视频列表（按照时间先后顺序）
1.  
2.  
3.  


#### 参与说明
1.  若想一同参与、贡献自身所闻、所知、所学之技术，debug将会很欢迎，可以加debug微信：debug0868 进行交流  
3.  


#### 联系debug  
1、debug的微信：debug0868   
![输入图片说明](https://images.gitee.com/uploads/images/2020/0807/165324_cb5c22f4_1442143.png "个人微信.png")  
2、debug的QQ: 1948831260

