package com.debug.java.technology.one.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class WorkerInfo implements Serializable{
    private Long id;

    private String code;

    private String name;

    private Date birthday;

    private Double money;

    private Integer sex;

    private Date createTime;

    public WorkerInfo(String code, String name, Date birthday, Double money) {
        this.code = code;
        this.name = name;
        this.birthday = birthday;
        this.money = money;
    }
}