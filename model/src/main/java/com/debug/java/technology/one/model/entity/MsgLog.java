package com.debug.java.technology.one.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class MsgLog implements Serializable{
    private String msgId;

    private String exchange;

    private String routingKey;

    private Integer status;

    private Integer tryCount;

    private Date nextTryTime;

    private Date createTime;

    private Date updateTime;

    private String msg;

    public MsgLog(String msgId, String exchange, String routingKey, Integer status, Integer tryCount, Date createTime, String msg) {
        this.msgId = msgId;
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.status = status;
        this.tryCount = tryCount;
        this.createTime = createTime;
        this.msg = msg;
    }
}