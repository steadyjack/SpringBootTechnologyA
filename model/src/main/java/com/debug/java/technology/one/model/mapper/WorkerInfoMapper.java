package com.debug.java.technology.one.model.mapper;

import com.debug.java.technology.one.model.entity.WorkerInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WorkerInfoMapper {
    int deleteByPrimaryKey(String code);

    int insert(WorkerInfo record);

    int insertSelective(WorkerInfo record);

    WorkerInfo selectByPrimaryKey(String code);

    int updateByPrimaryKeySelective(WorkerInfo record);

    int updateByPrimaryKey(WorkerInfo record);

    void insertBatch(@Param("datas") List<WorkerInfo> datas);
}