package com.debug.java.technology.one.model.mapper;


import com.debug.java.technology.one.model.entity.QrCode;

public interface QrCodeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(QrCode record);

    int insertSelective(QrCode record);

    QrCode selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrCode record);

    int updateByPrimaryKey(QrCode record);
}