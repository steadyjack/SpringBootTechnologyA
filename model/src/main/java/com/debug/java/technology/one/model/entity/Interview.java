package com.debug.java.technology.one.model.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Interview implements Serializable{
    private Integer id;

    private String name;

    private Integer age;

}