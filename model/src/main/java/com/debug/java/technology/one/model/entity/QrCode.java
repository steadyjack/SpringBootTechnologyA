package com.debug.java.technology.one.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QrCode implements Serializable {
    private Integer id;

    private String code;

    private Date createTime;

    public QrCode(String code) {
        this.code = code;
    }
}
