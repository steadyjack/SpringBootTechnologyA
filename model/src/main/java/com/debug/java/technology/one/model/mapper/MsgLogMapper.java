package com.debug.java.technology.one.model.mapper;

import com.debug.java.technology.one.model.entity.MsgLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MsgLogMapper {
    int deleteByPrimaryKey(String msgId);

    int insert(MsgLog record);

    int insertSelective(MsgLog record);

    MsgLog selectByPrimaryKey(String msgId);

    int updateByPrimaryKeySelective(MsgLog record);

    int updateByPrimaryKeyWithBLOBs(MsgLog record);

    int updateByPrimaryKey(MsgLog record);

    int updateMsgSendStatus(@Param("msgId") String msgId,@Param("status") Integer status);

    int updateMsgManageSuccess(@Param("msgId") String msgId,@Param("status") Integer status);

    Integer selectMaxTry(@Param("msgId") String msgId);

    int updateMaxTry(@Param("msgId") String msgId);

    List<MsgLog> selectSendFailMsg(@Param("status") String status);
}